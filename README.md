# Installation 

Il vous faut au moins :
- installer un serveur [wamp](https://www.wampserver.com)
- [Symfony 5](https://symfony.com/doc/current/cloud/getting-started.html#installing-the-cli-tool)
- [Composer](https://getcomposer.org/download/)


# Lancement

Pour lancer le projet, il faut le cloner a l'aide de l'invite de commande : 
```
cd fichier
git clone https://gitlab.univ-lr.fr/jmorga/sitegestiondeprojet.git siteGestionProjet 
cd siteGestionProjet
composer install
```

On crée le fichier .env.local en remplacant `login`, `motdepasse` par les identifiant de connexion au serveur mysql et `nomBasedeDonne` par le nom de la base de donnée : 
```
DATABASE_URL=mysql://login:motdepasse@localhost:3306/nomBasedeDonne
```
Ensuite on utilise la commande : `php bin/console doctrine:database:create`

Puis : `php bin/console make:migration`

Enfin : `php bin/console doctrine:migration:migrate`

On peut desormais lancer le site ! Grace a la commande :
`symfony server:start`
