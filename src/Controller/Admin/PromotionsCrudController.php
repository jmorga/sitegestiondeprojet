<?php

namespace App\Controller\Admin;

use App\Entity\Promotions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PromotionsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Promotions::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('Nom'),
            NumberField::new('taux'),
            BooleanField::new('active'),
            AssociationField::new('Categorie')->onlyOnIndex(),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::EDIT,'ROLE_ADMIN')
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            ->setPermission(Action::NEW,'ROLE_ADMIN');
    }

}
