<?php

namespace App\Controller\Admin;

use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\HiddenField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return User::class;
    }



    public function configureFields(string $pageName): iterable
    {
        return [
            EmailField::new('email','Email'),
            ChoiceField::new('roles', 'Roles')
                ->allowMultipleChoices()
                ->autocomplete()
                ->setChoices([
                    'User' => 'ROLE_USER',
                    'Moderateur' => 'ROLE_MODO',
                    'Admin' => 'ROLE_ADMIN'
                ])
            ->setPermission('ROLE_ADMIN'),
            TextField::new('adresseLivraison'),
            IntegerField::new('cpLivraison'),
            TextField::new('villeLivraison'),
            TextField::new('adresseFacturation'),
            IntegerField::new('cpFacturation'),
            TextField::new('villeFacturation'),
        ];
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->setPermission(Action::EDIT,'ROLE_ADMIN')
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            ->setPermission(Action::NEW,'ROLE_ADMIN');
    }

}
