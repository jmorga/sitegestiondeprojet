<?php

namespace App\Controller\Admin;

use App\Entity\Categorie;
use App\Entity\Commande;
use App\Entity\LigneCommande;
use App\Entity\Produit;
use App\Entity\Promotions;
use App\Entity\SousCategorie;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $produits = $this->getDoctrine()->getRepository(Produit::class)->count([]);
        $users = $this->getDoctrine()->getRepository(User::class)->count([]);

        return $this->render('default/dashboard.html.twig', [
            'produit' => $produits,
            'user' => $users,
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Shoppee !');
    }

    public function configureMenuItems(): iterable
    {

        yield MenuItem::linkToRoute("Site", "fa fa-home", 'homepage');
        yield MenuItem::linkToCrud('Produits', 'fas fa-shopping-cart', Produit::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-list', Categorie::class);
        yield MenuItem::linkToCrud('Sous-categories', 'fas fa-list', SousCategorie::class);
        yield MenuItem::linkToCrud('Clients', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Promotions', 'fas fa-percentage', Promotions::class);
        yield MenuItem::linkToCrud('Commande', 'fas fa-shopping-basket', Commande::class);
        yield MenuItem::linkToCrud('LigneCommande', 'fas fa-list-ul', LigneCommande::class);
    }
}
