<?php

namespace App\Controller\Admin;

use App\Entity\Produit;
use App\Form\ImagesProduitType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\MoneyField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ProduitCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Produit::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('Nom'),
            TextareaField::new('Description'),
            MoneyField::new('Prix' )->setCurrency('EUR'),
            AssociationField::new('Categorie'),
            AssociationField::new('SousCategorie') ->onlyOnForms(),
            CollectionField::new('SousCategorie')
                ->setTemplatePath('SousCategories.html.twig')
                ->onlyOnDetail(),
            BooleanField::new('visible')
            ->setPermission('ROLE_ADMIN'),
            NumberField::new('stock'),
            CollectionField::new('imageProduits')
                ->setLabel('Images du produits')
                ->setEntryType(ImagesProduitType::class)
                ->onlyOnForms(),
            CollectionField::new('imageProduits')
                ->setLabel('Images du produits')
                ->setTemplatePath('images.html.twig')
                ->onlyOnDetail(),
        ];
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, 'detail')
            ->setPermission(Action::EDIT,'ROLE_ADMIN')
            ->setPermission(Action::DELETE,'ROLE_ADMIN')
            ->setPermission(Action::NEW,'ROLE_ADMIN');
    }

}
