<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Commande;
use App\Entity\LigneCommande;
use App\Entity\Produit;
use App\Entity\Promotions;
use App\Entity\SousCategorie;
use App\Form\LigneCommandeType;
use App\Repository\PromotionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/content/{nomCat}", name="content")
     */
    public function content(string $nomCat): Response
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->findByName($nomCat)[0];
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findByCategorie($categorie->getId());

        return $this->render('default/content.html.twig', [
            'categorie' => $categorie,
            'produits' => $produits
        ]);
    }

    /**
     * @Route("/content/{nomCat}/{nomSousCat}", name="content2")
     */
    public function content2(string $nomCat, string $nomSousCat): Response
    {
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->findByName($nomCat)[0];
        $sousCat = $this->getDoctrine()->getRepository(SousCategorie::class)->findByName($nomSousCat)[0];
        $produitsSousCat = $this->getDoctrine()->getRepository(Produit::class)->findByCatSousCat($categorie->getId(), $sousCat->getId());

        return $this->render('default/content2.html.twig', [
            'categorie' => $categorie,
            'sousCat' => $sousCat,
            'produitsSousCat' => $produitsSousCat
        ]);
    }

    /**
     * @Route("/item/{id}", name="item")
     */
    public function item($id, Request $request): Response
    {
        $produit = $this->getDoctrine()->getRepository(Produit::class)->find($id);

        $ligne = new LigneCommande();
            $form = $this->createForm(LigneCommandeType::class, $ligne,
            );
        $form->handleRequest($request);
        $message="";

        if ($form->isSubmitted() && $form->isValid()) {
            if(  $produit->getStock()-$ligne->getQuantite()>=0){
                $produit->setStock($produit->getStock()-$ligne->getQuantite());
                if($produit->getStock()==0){
                    //mail("supply@shoppee.fr","rupture de stock sur ".$produit->getNom(),"rupture de stock sur ".$produit->getNom());
                }
                $commande = new Commande();
                $commande->setUser($this->getUser());
                $ligne->setProduit($produit);

                $em = $this->getDoctrine()->getManager();
                $em->persist($commande);
                $em->flush();




                $ligne->setCommande($commande);
                $em->persist($ligne);
                $em->flush();
                return $this->redirectToRoute('profil');
            }else{
                $message="Vous ne pouvez pas acheter plus que la quantité";
            }





        }

        $promotions = $this->getDoctrine()->getRepository(Promotions::class)->findAll();

        foreach ($promotions as $p ) {
            if (!empty($p->getActive())) {
                $cat = $p->getCategorie()[0];
                if ($cat == $produit->getCategorie()) {
                    $taux = $p->getTaux();
                } else {
                    $taux = 0;
                }
            } else {
                $taux = 0;
            }
        }

        return $this->render('default/item.html.twig', [
            'produit' => $produit,
            'taux' => $taux,
            'form' => $form->createView(),
            'message'=> $message,
        ]);
    }

    /**
     * @Route("/profil}", name="profil")
     */
    public function profil(): Response
    {
        $username = $this->getUser()->getUsername();
        $lignes = $this->getDoctrine()->getRepository(LigneCommande::class)->findByUser($username);

        return $this->render('default/profil.html.twig', [
            'username' => $username,
            'lignes' => $lignes
        ]);
    }

    /**
     * @Route("/promotion", name="promotion")
     */
    public function promotion(): Response
    {
        $promotions = $this->getDoctrine()->getRepository(Promotions::class)->findAll();

        foreach ($promotions as $p ) {
            if (!empty($p->getActive())) {
                $cat = $p->getCategorie()[0];
                $taux = $p->getTaux();
            }
        }

        $produits = $this->getDoctrine()->getRepository(Produit::class)->findByCategorie($cat->getId());
        return $this->render('default/promotion.html.twig', [
            'produits' => $produits,
            'taux' => $taux
        ]);
    }
}
