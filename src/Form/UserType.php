<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, ['label'=>'Email :'])
            ->add('password', PasswordType::class, ['label'=>'Mot de passe :'])
            ->add('adresseLivraison', TextType::class, ['label'=>'Adresse de livraison :'] )
            ->add('adresseFacturation', TextType::class, ['label'=>'Adresse de facturation :'] )
            ->add('cpLivraison', TextType::class, ['label'=>'Code postal de livraison:'])
            ->add('villeLivraison', TextType::class, ['label'=>'Ville de livraison:'])
            ->add('cpFacturation', TextType::class, ['label'=>'Code postal de facturatio:'])
            ->add('villeFacturation', TextType::class, ['label'=>'Ville de facturation'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
