<?php

namespace App\Entity;

use App\Repository\ImageProduitRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Date;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ImageProduitRepository::class)
 * @Vich\Uploadable()
 */
class ImageProduit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="products",fileNameProperty="image")
     */
    private $imageFile;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updateAt;


    /**
     * @ORM\ManyToOne(targetEntity=Produit::class, inversedBy="imageProduits")
     * @ORM\JoinColumn(nullable=false)
     */
    private $produit;

    public function __construct()
    {
        $this->createdAt=new \DateTime();
        $this->updateAt=new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image_name): self
    {
        $this->image = $image_name;

        return $this;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }
    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }
    public function setCreatedAt(\DateTimeInterface $creadtedAt): self
    {
        $this->createdAt = $creadtedAt;

        return $this;
    }
    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }
    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getImageFile(){
        return $this->imageFile;
    }
    /**
     * @param mixed $imageFile
     * @throws \Exception
     */
    public function setImageFile($imageFile):void{
        $this->imageFile=$imageFile;
        if($imageFile){
            $this->updateAt = new \DateTime();
        }
    }

    public function __toString()
    {
        return $this->getImage();
    }


}
